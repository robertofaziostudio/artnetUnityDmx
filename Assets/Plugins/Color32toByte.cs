﻿using UnityEngine;
using System;
using System.Runtime.InteropServices;

public class Color32toByte : MonoBehaviour 
{
	public static byte[] Color32ArrayToByteArray(Color32[] colors)
	{
		if (colors == null || colors.Length == 0)
			return null;
		
		int lengthOfColor32 = Marshal.SizeOf(typeof(Color32));
		int length = (lengthOfColor32 * colors.Length);

		byte[] bytes = new byte[length];
		GCHandle handle = default(GCHandle);
		try
		{
			handle = GCHandle.Alloc(colors, GCHandleType.Pinned);
			IntPtr ptr = handle.AddrOfPinnedObject();
			Marshal.Copy(ptr, bytes, 0, length);
		}
		finally
		{
			if (handle != default(GCHandle))
				handle.Free();
		}


	//	return bytes;

		int newLength = 24 * colors.Length;
		byte[] newBytes = new byte[newLength];
		
		int cont = 1;
		int newBytesPos = 0;
		for(int a = 0; a < length; a++)
		{
			if(cont%4!=0)
			{
				newBytes[newBytesPos++] = bytes[a];
			}
			cont++;
		}
		
		return newBytes;
	}
	

}



