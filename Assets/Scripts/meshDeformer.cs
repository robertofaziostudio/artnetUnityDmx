﻿using UnityEngine;
using System.Collections;

public class meshDeformer : MonoBehaviour {

	public Transform 	reference;
	public float 		displacementAmount;
	public float 		timeDumpener;
	private float		xs = 1;


	Mesh myMesh;
	public Vector3[] originalPoints;

	void Start () 
	{
		myMesh = GetComponent<MeshFilter>().mesh;
		originalPoints = new Vector3[ myMesh.vertexCount ];

		for( int i = 0; i< myMesh.vertexCount; i++ )
		{
			originalPoints[ i ] = new Vector3 ( myMesh.vertices[i].x, myMesh.vertices[i].y, myMesh.vertices[i].z );

		}
	}
	
	void Update () 
	
	{	

		float boom = GetSpectrumData.loudness;
		displacementAmount = boom;

		Vector3 refPoints = reference.position;
		Vector3[] newVert = myMesh.vertices;
		for( int i = 0; i< myMesh.vertexCount; i++)

		{
			Vector3 pt = originalPoints [ i ];
			Vector3 direction = ( pt - refPoints );
			float timeMul = ( pt - refPoints).magnitude * timeDumpener;

			direction.Normalize();

			newVert [i] = originalPoints [i] + direction * Mathf.Sin(Time.time * timeMul) * displacementAmount;


			//newVert [i] = originalPoints [i] + direction * Mathf.Sin(Time.time * timeMul) * displacementAmount;
			newVert [i] = originalPoints [i] + direction * Mathf.Sin(Time.time * timeMul) * GetSpectrumData.loudness;


		}

		myMesh.vertices =  newVert;
		float cr = Gui.hSliderValue;

	

		this.transform.localScale = new Vector3(xs, xs , xs);

	}

	void OnGUI () 
	{
		GUI.Box(new Rect(10,10,100,90), "Mesh Deformer");

		xs = GUI.HorizontalSlider (new Rect (25, 30, 70, 30), xs, 0.0f, 10.0f);

	}
}
