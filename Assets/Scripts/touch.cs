﻿using UnityEngine;
using System.Collections;

public class touch : MonoBehaviour 
{

	 public float speed = 1.0f;
	void Update () 
	{
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			transform.Translate(-Camera.main.transform.position.x * speed, -Camera.main.transform.position.y * speed, 0);
		}
	}
}
