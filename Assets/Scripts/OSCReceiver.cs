/* 
 * Classe per la ricezione dell'OSC
 */

using UnityEngine;
using System.Collections;

public class OSCReceiver : MonoBehaviour {
	
	public string			RemoteIP 				= "127.0.0.1";
	public int 				SendToPort 				= 57131;
	public int 				ListenerPort 			= 8338 ;

	private Osc 			handler;
	private UDPPacketIO 	udp;

	
	void Start () 
	{		
		udp = gameObject.AddComponent("UDPPacketIO") as UDPPacketIO;
		udp.init(RemoteIP, SendToPort, ListenerPort);
    	handler = gameObject.AddComponent("Osc") as Osc;
		handler.init(udp);
		

		handler.SetAddressHandler("/isadora/1", ListenEvent);	
		handler.SetAddressHandler("/1/volume1", ListenEvent);	

	}
	
	void Update () {
	
	}
	
	public void ListenEvent(OscMessage oscMessage)
	{	
		string address = oscMessage.Address;
	
		if(address == "/isadora/1")
		{
			float newValue = (float)System.Convert.ToSingle(oscMessage.Values[0]);
			Debug.Log (newValue);
		}

		if(address == "/1/volume1")
		{
			float newValue = (float)System.Convert.ToSingle(oscMessage.Values[0]);
			Debug.Log (newValue);
		}


		

	} 
}
