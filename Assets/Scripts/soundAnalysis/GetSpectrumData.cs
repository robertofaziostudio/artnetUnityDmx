﻿using UnityEngine;
using System.Collections;

/**
 * Roberto Fazio Studio 07.2014
 * Raw Microphone Sound Analisys 
 *
 * http://answers.unity3d.com/questions/394158/real-time-microphone-line-input-fft-analysis.html
 * http://answers.unity3d.com/questions/157940/getoutputdata-and-getspectrumDatadata-they-represent-t.html
 * http://forum.unity3d.com/threads/119595-Using-device-microphone-to-interact-with-objects
 * http://www.kaappine.fi/tutorials/fundamental-frequencies-and-detecting-notes/
 * 
 */

public class GetSpectrumData : MonoBehaviour 

{
	public static int 			deviceNum				= 2; 			// Select your input device number starting from 0
	public static int		    SAMPLECOUNT				= 1024;
	public int 					samplerate 				= 11024;
	public string 				CurrentAudioInput 		= "none";	
	public static float			loudness				= 0.0f;
	public float        		frequency				= 200.0f;
	public float				sensitivity				= 500.0f;
	public static float[]		spectrumData;							// Array of freq
	
	void Start () 
	{	// all audio devices available 
		string[] inputDevices = new string[Microphone.devices.Length];
		for(int i = 0; i < Microphone.devices.Length; i++)
		{	// print all 
			inputDevices[i] = Microphone.devices[i].ToString();

			Debug.Log ("Audio Device : " + inputDevices[i]);
		}

		CurrentAudioInput = Microphone.devices[deviceNum].ToString();
		spectrumData = new float[SAMPLECOUNT];
		// add current mic device to AudioSource component
		audio.clip = Microphone.Start (Microphone.devices [deviceNum], true, 999, samplerate);

		//HACK  - Wait for microphone to be ready, you can use this to control latency
		while (!(Microphone.GetPosition(Microphone.devices[deviceNum]) > 0))
		audio.Play();

	}
	public void OnGUI()
	{
		GUI.Label (new Rect (10, 400, 400, 400), CurrentAudioInput);
		GUI.Label (new Rect (10, 420, 400, 400), GetSpectrumData.loudness.ToString());
	}
	void Update() 
	{
		// Returns a block of the currently playing source's output data.
		audio.GetOutputData(spectrumData, 0);
		audio.GetSpectrumData(spectrumData, 0, FFTWindow.BlackmanHarris);
		loudness = GetAveragedVolume () * sensitivity ;
		frequency = GetFundamentalFrequency ();

	    if(!audio.isPlaying)
		audio.Play();

	}

	float GetAveragedVolume() 
	{
		float a = 0;
		foreach (float s in spectrumData) 
		{
			a += Mathf.Abs (s);
		}
		return a / 256;
	}

	float GetFundamentalFrequency() 
	{
		float fundamentalFrequency = 0.0f;
		float s = 0.0f;
		int i = 0;
		for (int j = 1; j < SAMPLECOUNT; j++) 
		{
			if (s < spectrumData [j]) 
			{
				s = spectrumData [j];
				i = j;
			}
		}
		fundamentalFrequency = i * samplerate / SAMPLECOUNT;
		return fundamentalFrequency;
	}
}
