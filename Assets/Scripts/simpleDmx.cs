﻿using UnityEngine;
using System;
using System.Collections;
using ArtNet;


public class simpleDmx : MonoBehaviour {

	public Color32 col;
	//creo un array di 512 byte per inserirci il messaggio:
	public byte[] DMXData = new byte[512];
	public float[] spect;

	ArtNet.Engine ArtEngine; 
	
	// Use this for initialization
	void Start () 
	{
		//istanzio il gestore della comunicazione:
		ArtEngine = new ArtNet.Engine("Open DMX Etheret", "255.255.255.255");
		//apro il canale di comunicazione con i parametri inseriti sopra:
		ArtEngine.Start ();
	}
	
	// Update is called once per frame
	void Update () 
	{	

		/*

		//Debug.Log(col.ToString());
		spect = GetSpectrumData.spectrumData;
		for ( int i = 0; i< DMXData.Length; i++)
		{
			DMXData[i] = (byte)ReMap.Remap(spect[i]*100000, 0, 5, 0, 255);;
		}
*/
		renderer.material.color = col;
		//test
	

		DMXData[0] = col.r;
		DMXData[1] = col.g;
		DMXData[2] = col.b;

		//mando il messaggio:
		ArtEngine.SendDMX (0, DMXData, DMXData.Length);
		// test2
	
	}

	void OnApplicationQuit ( )
	{	
		for (int i = 0; i< DMXData.Length; i++) 
		{	//set all light to black on quit application

			byte[] setBlack = new byte[512];
			ArtEngine.SendDMX (0, setBlack, setBlack.Length);
		}
	}
			
}
